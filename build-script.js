const fs = require('fs-extra');
const concat = require('concat'); 
(async function build() {    
const files = [
        './dist/fitness-kit/polyfills-es5.js',
        './dist/fitness-kit/main-es5.js',
        './dist/fitness-kit/0-es5.js',
        './dist/fitness-kit/5-es5.js',
        './dist/fitness-kit/6-es5.js',
        './dist/fitness-kit/7-es5.js',  
        './dist/fitness-kit/8-es5.js',  
]
await fs.ensureDir('builder/widgets')    
await concat(files, 'builder/widgets/fitness-kit-card.js')})()