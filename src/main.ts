import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { environment } from './environments/environment';
import { ScheduleModuleModule } from './app/schedule-module.module';

if (environment.production) {
  enableProdMode();
}



platformBrowserDynamic().bootstrapModule(ScheduleModuleModule)
  .catch(err => console.error(err));

// String.prototype.replaceAll = function (search, replace) {
//     return this.split(search).join(replace);
// }