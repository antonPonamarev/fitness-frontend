import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { skip } from 'rxjs/operators';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { PersonalService } from './services/personal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements 
AfterViewInit {
  title = 'fitness-kit';
  name = 'Set iframe source';
  url: string = "https://securepayments.sberbank.ru/payment/merchants/sbersafe/payment_ru.html?mdOrder=6cc007b9-ecf4-79d2-9c9c-93d501f9ff9c";
  urlSafe: SafeResourceUrl;

  interval: any

  constructor(public sanitizer: DomSanitizer, private ps:PersonalService) { }
  @ViewChild("mainScreen") elementView: ElementRef;
  ngOnInit() {

    
    this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }
  ngAfterViewInit(){
    setInterval(
      ()=>{     
      if(document.body.offsetHeight != 0) {
      parent.postMessage(document.body.offsetHeight, '*');
      clearInterval(this.interval)
    }  
  },100)
   
  }

  public changeUrl(event){
  }
}
