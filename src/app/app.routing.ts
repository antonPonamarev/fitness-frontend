
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AdminLayoutModule } from './layouts/admin-layout/admin-layout.module';
import { WidgetsLayoutModule } from './layouts/widgets-layout/widgets-layout.module';
import { PersonalLayoutModule } from './layouts/personal-layout/personal-layout.module';
import { WidgetsLayoutComponent } from './layouts/widgets-layout/widgets-layout.component';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { PersonalLayoutComponent } from './layouts/personal-layout/personal-layout.component';
import { LoginPageComponent } from './pages/personal/login-page/login-page.component';

const routes: Routes =[
    {
      path: '',
      redirectTo: 'widget/shop',
      pathMatch: 'full',
    },
    {
      path: '',
      component: AdminLayoutComponent,
      children: [
        {
          path: '',
          loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
        }
      ]
    }, 
    {
      path: 'modal',
      component: PersonalLayoutComponent ,
      outlet: 'modal',
      children: [
        {
          path: '',
          loadChildren: './layouts/personal-layout/personal-layout.module#PersonalLayoutModule'
        }
      ]
    }, 
    {
      path: 'widget',
      component: WidgetsLayoutComponent,
      children: [
        {
          path: '',
          loadChildren: './layouts/widgets-layout/widgets-layout.module#WidgetsLayoutModule'
        }
      ]
    },  
      
    {
      path: '**',
      redirectTo: 'widget/shop'
    }
  ];
  
  @NgModule({
    imports: [
      CommonModule,
      BrowserModule,
      RouterModule.forRoot(routes,{
        useHash: true
      })
    ],
    exports: [
    ],
  })
  export class AppRoutingModule { }