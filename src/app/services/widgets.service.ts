import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, defer } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Trainer } from '../classes/trainer';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { Lesson } from '../classes/lesson';
import { Service } from '../classes/service';
import { LessonMatrix } from '../classes/lesson-matrix';
import { Membership } from '../classes/membership';
import { ActivatedRoute } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class WidgetsService {

  constructor(private http: HttpClient, private ar: ActivatedRoute ) { }
  

  public currentCards: any = {}
  public getCurrentCards(){
    return this.currentCards
  }


  public waitCardById(id:string):Boolean{
    return this.currentCards[id] !== undefined
  }

  public getCardById( id:string):Membership{
    return this.currentCards[id] !== undefined ? this.currentCards[id] : new Membership()
  }




  
  public getTrainers(): Pending<Lesson[]> {
    const status = new ReplaySubject<Status>();
    status.next(Status.LOADING);
    const request = this.http
      .get<any>(`https://clubis.fitnesskit-admin.ru/schedule/get_v3/?start_date=2020-7-13&end_date=2020-7-20&club_id=1`)
      .pipe(
        map(data => data.trainers.map(
          obj => Object.assign(
            new Trainer(), obj
            )
        )),
        retry(2),
        catchError(error => {
          status.next(Status.ERROR);
          throw 'error loading user';
        }),
        tap(() => status.next(Status.SUCCESS))
      );

    const data = defer(() => {
      status.next(Status.LOADING);
      return request;
    });

    return { data, status };
  }

  public getCard(id:string): Pending<Membership[]> {
    const status = new ReplaySubject<Status>();
    status.next(Status.LOADING);
    const request = this.http
      .get<any>(`https://olimpia.fitnesskit-admin.ru/goods/get_club_cards/2/`)
      .pipe(
        map(data => {
          let result = data.memberships.filter(
            obj=>obj.id == id
            ).map(
          obj => Object.assign(
            new Membership(), obj
            )
          )
          console.log(result)
        return result
        }),
        retry(2),
        catchError(error => {
          status.next(Status.ERROR);
          throw 'error loading user';
        }),
        tap(() => status.next(Status.SUCCESS))
      );

    const data = defer(() => {
      status.next(Status.LOADING);
      return request;
    });

    return { data, status };
  }

  public getCards(): Pending<Membership[]> {
    const status = new ReplaySubject<Status>();
    status.next(Status.LOADING);
    const request = this.http
      .get<any>(`https://olimpia.fitnesskit-admin.ru/goods/get_club_cards/2/`)
      .pipe(
        map(data =>{
       
          let temp = data.memberships.map(
          obj => {
           let card:Membership =  Object.assign(
            new Membership(), obj
            )
    
            this.currentCards[card.id] = card
            return card

          }

          
        )
        parent.postMessage(document.body.offsetHeight, '*');
        
        return temp
          }),
        retry(2),
        catchError(error => {
          status.next(Status.ERROR);
          throw 'error loading user';
        }),
        tap(() => status.next(Status.SUCCESS))
      );

    const data = defer(() => {
      status.next(Status.LOADING);
      return request;
    });

    return { data, status };
  }


  getTrainerService(coach_id:string): Pending<Service[]> {
    const status = new ReplaySubject<Status>();
    status.next(Status.LOADING);
    const request = this.http
      .post<any>(`https://clubis.fitnesskit-admin.ru/team/get_coach_services_for_purchase/`,JSON.stringify({coach_id:coach_id,club_id:1}))
      .pipe(
        map((r:Response) => {
          return r["products"].map(obj => Object.assign(
            new Service(), obj
            ))
        }),
        retry(2),
        catchError(error => {
          status.next(Status.ERROR);
          throw 'error loading user';
        }),
        tap(() => status.next(Status.SUCCESS))
      );

    const data = defer(() => {
      status.next(Status.LOADING);
      return request;
    });

    return { data, status };
  }

}

export interface Pending<t> {
  data: Observable<t>;
  status: Observable<Status>;
}

export interface TrainerInterface{
  description: string
  full_name: string
  id: string
  image_url: string
  image_url_medium:string
  image_url_small: string
  last_name: string
  name: string
  position: string
}

export interface WidgetEntity{
 //  data: any;
  // data: any;
   //option: any,
   //tabs: any,
   schedule:LessonMatrix,
   lessons: Lesson[],
   trainers: Trainer[],
   tabs: any[]
}

export enum Status {
  LOADING = 'LOADING',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR'
}

