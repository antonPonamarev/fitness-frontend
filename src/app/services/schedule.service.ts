import { Injectable } from '@angular/core';
import { Status, WidgetEntity, Pending } from './widgets.service';
import { LessonMatrix } from '../classes/lesson-matrix';
import { Lesson } from '../classes/lesson';
import { Observable, ReplaySubject, defer } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { catchError, retry, tap } from 'rxjs/operators';
import { Trainer } from '../classes/trainer';
import { Week } from '../classes/time-manager';
import { Service } from '../classes/service';
@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) { }



  public getHeaders():HttpHeaders{
    let headers:HttpHeaders = new HttpHeaders()

   if(localStorage.getItem('token') !== null){
     headers= new HttpHeaders({
       token:localStorage.getItem('token')
     })
 
   }  
 
   return headers
 }



  public applyForGroupLesson(l: Lesson):Observable<Boolean>{
    return this.http
      .post<WidgetEntity>(`/schedule/apply_for_group_lesson/`, JSON.stringify(l.getIds()), {
        withCredentials:true,
        headers: this.getHeaders(),
      })
      .pipe(
        map(data => {
            return false;
        }),
        retry(2),
        catchError(error => {
          throw 'error loading user';
        })
      );
  }

  getTrainerService(coach_id:string): Pending<Service[]> {
    const status = new ReplaySubject<Status>();
    status.next(Status.LOADING);
    const request = this.http
      .post<any>(`/team/get_coach_services_for_purchase/`,JSON.stringify({coach_id:coach_id,club_id:1}))
      .pipe(
        map((r:Response) => {
          return r["products"].map(obj => Object.assign(
            new Service(), obj
            ))
        }),
        retry(2),
        catchError(error => {
          status.next(Status.ERROR);
          throw 'Ошибка загрузки услуг';
        }),
        tap(() => status.next(Status.SUCCESS))
      );

    const data = defer(() => {
      status.next(Status.LOADING);
      return request;
    });

    return { data, status };
  }

  public getSchedule(w:Week): Pending<WidgetEntity> {
    const status = new ReplaySubject<Status>();
    status.next(Status.LOADING);
    const request = this.http
      .get<WidgetEntity>(`/schedule/get_v3/?start_date=${w.start}&end_date=${w.end}&club_id=1`)
      .pipe(
        map(data => {


          let trainers:any = {},
          schedule: LessonMatrix = new LessonMatrix(10)
          

          data.trainers.forEach(trainer => {
              trainers[trainer["id"]] = Object.assign(new Trainer(), trainer)
          })

          let lessons:Lesson[] = data.lessons.map(lesson => {
                let l:Lesson = Object.assign(new Lesson(), lesson)
                l.addCoach(trainers[lesson["coach_id"]])
                return l
          })
          schedule.addArray(lessons)
          console.log(schedule)
            return {
              schedule,
              lessons,
              trainers,
              tabs:data.tabs
            }
        }),
        retry(2),
        catchError(error => {
          status.next(Status.ERROR);
          throw 'error loading user';
        }),
        tap(() => status.next(Status.SUCCESS))
      );

    const data = defer(() => {
      status.next(Status.LOADING);
      return request;
    });

    return { data, status };
  }
}
