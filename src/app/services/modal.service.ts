import { Injectable } from '@angular/core';
import { Lesson } from '../classes/lesson';
import { ModalType, ModalEntity } from '../components/modal/common-modal/common-modal.component';
import { Trainer } from '../classes/trainer';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }
  public isOpened:Boolean = false


  public entity:ModalEntity = {
    type : ModalType.empty,
  }
  
  public getEntity(){
    return this.entity
  }

  public closeModal():void{
    this.isOpened = false
  }

  public openLesson(lesson:Lesson){
    this.entity = {
      type:ModalType.lesson,
      lesson
    }
    this.isOpened = true
    console.log(this.entity)
  }

  public openTrainer(trainer:Trainer){
    this.entity = {
      type:ModalType.trainer,
      trainer
    }
    console.log(trainer)
    this.isOpened = true
  }
}
