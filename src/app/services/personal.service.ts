import { Injectable } from '@angular/core';
import { Pending, Status } from './widgets.service';
import { ReplaySubject, defer, Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { retry, catchError, tap, map, finalize } from 'rxjs/operators';
import { UrlTree, Router, RouterStateSnapshot, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { Membership } from '../classes/membership';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { TimeManager } from '../classes/time-manager';
import { Club } from '../classes/club';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  constructor(private http: HttpClient,private router: Router,private ar: ActivatedRoute) { }

 


public getHeaders():HttpHeaders{
   let headers:HttpHeaders = new HttpHeaders()
   console.log(localStorage.getItem('token'))
  if(localStorage.getItem('token') !== null){
    headers= new HttpHeaders({
      token:localStorage.getItem('token')
    })

  }  

  return headers
}

public testCookie(): Observable<any> {
  return  this.http
    .post<any>(`/cookie.php`,{
      withCredentials:true,
      headers: this.getHeaders(),
    })
    .pipe(
      retry(2),
      catchError(error => {
 
        throw 'error loading user';
      })
    );


}

  public getSecretCode(phone:string): Observable<any> {
    return  this.http
      .post<any>(`https://olimpia.fitnesskit-admin.ru/accounts/get_secret_number/`,JSON.stringify({phone}),{
        withCredentials:true,
        headers: this.getHeaders(),
      })
      .pipe(
        retry(2),
        catchError(error => {
   
          throw 'error loading user';
        })
      );


  }

  public sendSecretCode(phone:string,code:string,club_id:string): Observable<any> {

    return  this.http
      .post(`https://olimpia.fitnesskit-admin.ru/accounts/login_by_sms_to_club/`,JSON.stringify({phone, code,club_id}),{
        withCredentials:true,
        headers: this.getHeaders(),
        observe: 'response'
      })
      .pipe(
     
        catchError(error => {
          throw 'error loading user';
        })
      );


  }

  public checkPayment(next: ActivatedRouteSnapshot): Observable <any>{
    
    return  this.http
    .get(`https://olimpia.fitnesskit-admin.ru/accounts/verification_status/`,{
      withCredentials:true,
      headers: this.getHeaders(),
    })
    .pipe(
      map((r:Response) => {
        if(r['status'].toString() == 'NotVerified'){
          //this.router.navigate([{outlets: {['modal']:['modal','verification','user']}}],{queryParams:this.ar.snapshot.queryParams});
          
          return this.ar.snapshot.queryParamMap.has('backurl') ? this.router.createUrlTree([{outlets: {['modal']:['modal','verification','user']}}],{queryParams:this.ar.snapshot.queryParams}) : this.router.createUrlTree([{outlets: {['modal']:['modal','verification','user']}}],{queryParams:{backurl:'modal/'+next.url.join('/')}})
        }
        else{
          return true
        }
        
      }),
      tap(res =>{
        console.log("ok")
        return true
      },
      err=>{
        this.router.navigate([{outlets: {['modal']:['modal','login']}}],{queryParams:{backurl:'modal/'+next.url.join('/')}});
        return true}
      )
    );
  }


  public getPaymentLink(card:Membership): Observable <string>{
    
    return  this.http
    .post(`https://olimpia.fitnesskit-admin.ru/payment/request_payment_form_v2/`,{
        'service_id' : card.id,
        'amount' : card.price,
       // 'amount' : 2,
        'club_id' : 2,
        'return_url' : 'https://olimpia.fitnesskit-admin.ru/main/finish.html',
        'fail_url' : 'https://olimpia.fitnesskit-admin.ru/main/error.html'
    },{
      withCredentials:true,
      headers: this.getHeaders(),
    })
    .pipe(
      map((r:Response) => {
        return r['base_url'];
      })

    );
  }

  public sendEmptySelfi(): Observable <any>{
    
    return  this.http
    .post(`https://olimpia.fitnesskit-admin.ru/accounts/save_selfie/`,{
      "use_avatar": true
    },{
      withCredentials:true,
      headers: this.getHeaders(),
    }).pipe();
  }


  public getUser(): Pending<any> {
    const status = new ReplaySubject<Status>();
    status.next(Status.LOADING);
    const request = this.http
      .get<any>(`https://olimpia.fitnesskit-admin.ru/accounts/get_user_info`,{
        withCredentials:true,
        headers: this.getHeaders(),
      })
      .pipe(
        map((r)=>{
         // r.birth_date = TimeManager.dateConverterToView(r.birth_date)
          return r
        }),
        retry(2),
        catchError(error => {
          status.next(Status.ERROR);
          throw 'error loading user';
        }),
        tap(() => status.next(Status.SUCCESS))
      );

    const data = defer(() => {
      status.next(Status.LOADING);
      return request;
    });

    return { data, status };
  }

  public getClubs(): Pending<Club[]> {
    const status = new ReplaySubject<Status>();
    status.next(Status.LOADING);
    const request = this.http
      .get<any>(`https://olimpia.fitnesskit-admin.ru/clubs/get/`)
      .pipe(
        map(data => data.map(
          obj => Object.assign(
            new Club(), obj
            )
        )),
        retry(2),
        catchError(error => {
          status.next(Status.ERROR);
          throw 'error loading user';
        }),
        tap(() => status.next(Status.SUCCESS))
      );

    const data = defer(() => {
      status.next(Status.LOADING);
      return request;
    });

    return { data, status };
  }

  


  public updateUser(user:any):Observable<any> {
let userObj = {}
//user.birth_date = TimeManager.dateConverterToDb(user.birth_date)
for(let key in user){
  if(key == "image") continue
  userObj[key] = user[key]
}

  
    return this.http
      .post<any>(`https://olimpia.fitnesskit-admin.ru/accounts/change_user_data/`,userObj,{
        withCredentials:true,
        headers: this.getHeaders(),
      })
      
      .pipe(
        retry(2),
        catchError(error => {
          throw error.error.email[0];
        })

      );

  }

  public uploadUserPassport(event, address:string): Observable<any> {

    const target = event.target as HTMLInputElement;
    const selectedFile = target.files[0];

    const uploadData = new FormData();
    uploadData.append('image', selectedFile, selectedFile.name);
    uploadData.append('address', address);
    return this.http.post('https://olimpia.fitnesskit-admin.ru/accounts/save_passport/', uploadData, {
            reportProgress: true, // Без observe: 'events' не работает
            withCredentials:true,
            headers: this.getHeaders(),
            observe: 'events', // без reportProgress: true только HttpEventType.Sent и HttpEventType.Response
          })
  }

  user:any = {
    id : -1
  }

  public userChange(id){
    this.user.id = id
  }
  public getUserFull(){
    return this.user
  }

  public uploadUserSelfi(event): Observable<any> {

    const target = event.target as HTMLInputElement;
    const selectedFile = target.files[0];

    const uploadData = new FormData();
    uploadData.append('image', selectedFile, selectedFile.name);

    return this.http.post('https://olimpia.fitnesskit-admin.ru/accounts/save_selfie/', uploadData, {
            reportProgress: true, // Без observe: 'events' не работает
            withCredentials:true,
            headers: this.getHeaders(),
            observe: 'events', // без reportProgress: true только HttpEventType.Sent и HttpEventType.Response
          })
  }

  public uploadUserImage(event,user): Observable<any> {

    const target = event.target as HTMLInputElement;
    const selectedFile = target.files[0];

    const uploadData = new FormData();
    uploadData.append('image', selectedFile, selectedFile.name);
    for(let key in user){
      if(key == "image") continue
      uploadData.append(key,user[key]);
    }
    return this.http.post('https://olimpia.fitnesskit-admin.ru/accounts/change_user_data/', uploadData, {
            reportProgress: true, // Без observe: 'events' не работает
            withCredentials:true,
            headers: this.getHeaders(),
            observe: 'events', // без reportProgress: true только HttpEventType.Sent и HttpEventType.Response
          })
  }

  public uploadFile(event): void {

    const target = event.target as HTMLInputElement;
    const selectedFile = target.files[0];

    const uploadData = new FormData();
    uploadData.append('image', selectedFile, selectedFile.name);
   // uploadData.append('image12',"1212");

    this.http.post('https://olimpia.fitnesskit-admin.ru/accounts/save_selfie/', uploadData, {
            reportProgress: true, // Без observe: 'events' не работает
            withCredentials:true,
            headers: this.getHeaders(),
            observe: 'events', // без reportProgress: true только HttpEventType.Sent и HttpEventType.Response
          }).subscribe((r:any) => {
                console.log("photo")
            if(r.type == 1){
             
            }
            if(r.body !== undefined){
  
        
            }

          })
  }

}
