import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonsWrapperComponent } from 'src/app/wrapper/lessons-wrapper/lessons-wrapper.component';
import { LessonsPageComponent } from 'src/app/pages/widgets/lessons-page/lessons-page.component';
import { LessonComponent } from 'src/app/components/table/lesson/lesson.component';
import { AppBadgeComponent } from 'src/app/components/common/app-badge/app-badge.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LessonModalComponent } from './components/modal/lesson-modal/lesson.component';
import { TrainerModalComponent } from './components/modal/trainer-modal/trainer-modal.component';
import { CommonModalComponent } from './components/modal/common-modal/common-modal.component';
import { ModalService } from './services/modal.service';



@NgModule({
  declarations: [
    LessonsWrapperComponent,
    LessonsPageComponent,
    LessonComponent,
    CommonModalComponent,
    LessonModalComponent,
    TrainerModalComponent,
    AppBadgeComponent,
  ],
  imports: [
    RouterModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModule
  ],
  providers: [ModalService],
  bootstrap: [LessonsWrapperComponent]
})
export class ScheduleModuleModule { }
