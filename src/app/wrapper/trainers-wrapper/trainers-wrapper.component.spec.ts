import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainersWrapperComponent } from './trainers-wrapper.component';

describe('TrainersWrapperComponent', () => {
  let component: TrainersWrapperComponent;
  let fixture: ComponentFixture<TrainersWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainersWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainersWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
