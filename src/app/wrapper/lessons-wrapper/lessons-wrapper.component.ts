import { Component, OnInit } from '@angular/core';
import { Pending, WidgetsService, WidgetEntity, Status } from 'src/app/services/widgets.service';
import { LessonMatrix } from 'src/app/classes/lesson-matrix';
import { ScheduleService } from 'src/app/services/schedule.service';
import { TimeManager } from 'src/app/classes/time-manager';

@Component({
  selector: 'app-lessons-wrapper',
  templateUrl: './lessons-wrapper.component.html',
  styleUrls: ['./lessons-wrapper.component.css']
})
export class LessonsWrapperComponent implements OnInit {

  constructor(private ts: ScheduleService) { }

  public widget:Pending<WidgetEntity>
  public Status = Status


  ngOnInit(): void {
    this.widget = this.ts.getSchedule(TimeManager.getWeek())
  }

}
