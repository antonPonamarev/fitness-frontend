import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TrainerPageComponent } from 'src/app/pages/widgets/trainer-page/trainer-page.component';


export const AdminLayoutRoutes: Routes = [
  { path: 'admin',      component: TrainerPageComponent },
  { path: 'admin10',      component: TrainerPageComponent },
];







@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(AdminLayoutRoutes),
    CommonModule
  ]
})
export class AdminLayoutModule { }
