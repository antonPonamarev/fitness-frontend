import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TrainerComponent } from 'src/app/components/table/trainer/trainer.component';



import { WidgetsService } from '../../services/widgets.service';


import { TrainersWrapperComponent } from 'src/app/wrapper/trainers-wrapper/trainers-wrapper.component';
import { TrainerPageComponent } from 'src/app/pages/widgets/trainer-page/trainer-page.component';


import { ShopPageComponent } from 'src/app/pages/widgets/shop-page/shop-page.component';

import { CardModalComponent } from 'src/app/components/modal/card-modal/card-modal.component';
import { ErrorComponent } from 'src/app/components/common/error/error.component';
import { UserComponent } from 'src/app/components/common/user/user.component';
import { CardModule } from 'src/app/modules/card/card.module';
import { PaymentFinishComponent } from 'src/app/pages/personal/payment-finish/payment-finish.component';
import { PersonalService } from 'src/app/services/personal.service';
import { ModalModuleModule } from 'src/app/modules/modal-module/modal-module.module';





export const WidgetLayoutRoutes: Routes = [
  //{ path: 'schedule',      component: LessonsWrapperComponent },
  { path: 'trainers',      component: TrainerPageComponent },
  { path: 'shop',      component: ShopPageComponent },
  { path: 'shop/:id',      component: ShopPageComponent },
  { path: 'finish', component: PaymentFinishComponent}
];

@NgModule({
  declarations: [
    TrainerComponent,
    ShopPageComponent,
    CardModalComponent,
    TrainerPageComponent,
    TrainersWrapperComponent,
    ErrorComponent,
    UserComponent
  ],
  imports: [
    RouterModule.forChild(WidgetLayoutRoutes),
    CommonModule,
    ModalModuleModule,
    CardModule
  ],

})
export class WidgetsLayoutModule { }
