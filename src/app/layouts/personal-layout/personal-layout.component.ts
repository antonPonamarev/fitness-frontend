import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ChildActivationEnd } from '@angular/router';

@Component({
  selector: 'app-personal-layout',
  templateUrl: './personal-layout.component.html',
  styleUrls: ['./personal-layout.component.css']
})
export class PersonalLayoutComponent implements OnInit {

  constructor(private router: Router) { }

  currentOverflow:string = "auto"

  ngOnInit(): void {

    
    this.router.events.subscribe(event => {

      if (event instanceof ChildActivationEnd) {
        if(event.snapshot.outlet == "modal"){
          this.currentOverflow = document.body.style.overflow
          document.body.style.overflow = "hidden"
        }
        
      }
    })

  }


  
  public close():void{
    console.log(this.currentOverflow)
    this.router.navigate([{outlets: {['modal']:null}}])
    document.body.style.overflow = this.currentOverflow != "" &&  this.currentOverflow != "hidden"? this.currentOverflow : "auto"
  }

}
