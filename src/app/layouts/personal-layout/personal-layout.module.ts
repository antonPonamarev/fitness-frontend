import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from 'src/app/pages/personal/login-page/login-page.component';
import { Routes, RouterModule } from '@angular/router';
import { VerificationPhotoComponent } from 'src/app/pages/personal/verification-photo/verification-photo.component';
import { VerificationDocumentComponent } from 'src/app/pages/personal/verification-document/verification-document.component';
import { FormsModule } from '@angular/forms';
import { VerificationUserComponent } from 'src/app/pages/personal/verification-user/verification-user.component';
import { PaymentComponent } from 'src/app/pages/personal/payment/payment.component';
import { PaymentGuard } from 'src/app/guards/personal/payment.guard';
import { MainPersonalPageComponent } from 'src/app/pages/personal/main-personal-page/main-personal-page.component';
import { CardModule } from 'src/app/modules/card/card.module';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { WidgetsService } from 'src/app/services/widgets.service';
import { PersonalService } from 'src/app/services/personal.service';
import { PaymentPreloaderComponent } from 'src/app/pages/personal/payment-preloader/payment-preloader.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};
export const PersonalLayoutRoutes: Routes = [{
    path: 'login',
    component: LoginPageComponent

  },
  {
    path: 'verification/photo',
    component: VerificationPhotoComponent
  },
  {
    path: 'verification/document',
    component: VerificationDocumentComponent
  },
  {
    path: 'verification/user',
    component: VerificationUserComponent
  }
  ,
  {
    path: 'payment/empty/:id',
    component: PaymentPreloaderComponent
  }
  ,
  
  {
    path: 'payment/:type/:id',
    component: PaymentComponent,
    canActivate: [PaymentGuard]
  }
  ,
  {
    path: 'personal/main',
    component: MainPersonalPageComponent
  }
];

@NgModule({
  declarations: [
    LoginPageComponent,
    MainPersonalPageComponent,
    VerificationPhotoComponent,
    PaymentComponent,
    VerificationUserComponent,
    PaymentPreloaderComponent,
    VerificationDocumentComponent
  ],

  imports: [
    CommonModule,
    FormsModule,
    NgxMaskModule.forRoot(options),
    RouterModule.forChild(PersonalLayoutRoutes),
    CardModule
  ]
})
export class PersonalLayoutModule {}
