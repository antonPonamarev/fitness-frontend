import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TestLessonComponent } from '../components/test-lesson/test-lesson.component';

import { TrainerModule } from './trainer.module';




 

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    TrainerModule
  ],
  declarations: [
    TestLessonComponent,

  ],
  providers: [],
  bootstrap: [TestLessonComponent]
})
export class CardModule { }
