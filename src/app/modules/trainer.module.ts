import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TestTrainerComponent } from '../components/test-trainer/test-trainer.component';




 

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  declarations: [
    TestTrainerComponent,

  ],
  providers: [],
  bootstrap: [TestTrainerComponent]
})
export class TrainerModule { }
