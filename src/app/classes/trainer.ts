import { Service } from './service'
import { Pending } from '../services/widgets.service'
import { Observable } from 'rxjs'

export class Trainer {

    //public show:Boolean = false
    public full_name:string
    public position:string
    public image_url:string = ""
    
    public  description:string

    public id:string

    public image_url_medium:string
    public image_url_small:string
    public last_name:string
    public name:string
    public service: Pending<Service[]>

    public getAvatarStyle(){
        return {
            backgroundImage : `url(${this.image_url})`
        }
    }


    public static create(obj){
        let t:Trainer = new Trainer()
        for(let key in obj){
            t[key] = obj[key]
        }
        return t 
    }
}
