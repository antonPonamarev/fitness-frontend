export class Membership {
    id:string = '-1'
    name:string
    description:string = ""
    price:number
    freeze_days_count:string
    days_count:string
    old_price:number
}
