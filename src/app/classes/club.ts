export class Club {
    address:string = ""
    email:string = ""
    fb_news_domain:string = ""
    id:number = 1
    instagram:string = ""
    latitude:number = 0
    longitude:number = 0
    name:string = "Клуб"
    phone:string = ""
    twitter:string = ""
    vk_news_domain:string = ""
    website:string = ""
    work_hours:string = ""
}
