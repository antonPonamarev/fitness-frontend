export class TimeManager {
    
    public static weekDaysText:string[] = ['Понедельник','Вторник','Среда','Четверг','Пятница','Cуббота','Воскресенье']

    public static getStartWeek():Date{
        let d:Date = new Date(),
        currentDay:number = d.getDay() == 0 ? 7 : d.getDay()
        d.setDate(d.getDate() - currentDay + 1) 
        return  d
    }

    public static getWeek():Week{

        let d:Date = this.getStartWeek()
        let start:string = `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()}`
        d.setDate(d.getDate() + 6)
        let end:string = `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()+1}`

        return  {
            start,
            end
        }

    }
    
    

    public static getWeekDays():Day[]{
        let d:Date = this.getStartWeek(),
        week:Day[] = []
        
        for(let i = 0; i< 7; i++){
            d.setDate(d.getDate() + 1)
            week.push(
                {
                    number: d.getDate().toString(),
                    text: this.weekDaysText[i]
                }
            )
        }
        return week
    }


    public static getDate(strDate:string):number{
        let d:Date = new Date(strDate)
        return d.getDate()
    }


    public static dateConverterToView(date:string){
        let d:Date = new Date(date)
        return `${this.pullLeft(d.getDate())}.${this.pullLeft(d.getMonth()+1)}.${d.getFullYear()}`
    }

    public static dateConverterToDb(date:string){
        console.log(date)
        let datePart:string[] = date.split('.')
        let d:Date = new Date( parseInt(datePart[2]), parseInt(datePart[1])-1,parseInt(datePart[0]))
        console.log(d)
        console.log(`${d.getFullYear()}-${this.pullLeft(d.getMonth()+1)}-${this.pullLeft(d.getDate())}`)
        return `${d.getFullYear()}-${this.pullLeft(d.getMonth()+1)}-${this.pullLeft(d.getDate())}`
    }

    private static pullLeft(t:number){
        return t.toString().length == 1 ? `0${t}` : t
    }
}


export interface Week{
    start:string,
    end:string
}

export interface Day{
    number:string,
    text:string
}