import { Trainer } from './trainer'

export class Lesson {


  appointment_id: string
  available_slots: number
  client_recorded: boolean
  coach: Trainer
  color: string
  commercial: boolean
  date: string
  full_date: Date
  description: string
  endTime: Date
  name: string
  place: string
  service_id: string
  startTime: string
  tab: string
  tab_id: number
  coach_id: string = ""

  public addCoach(t:Trainer):void{
    this.coach = t
  }


  public getDate(){
    this.full_date = new Date(this.date)
    return this.full_date.getDate() 
  }

public getColor(){
  return {
    background : this.color
  }
}

public getIds(){
  return {
    "appointment_id" : this.appointment_id,
    "service_id" : this.service_id,
    commercial:this.commercial,
    
  }
}

}
