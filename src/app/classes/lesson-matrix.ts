import { Lesson } from './lesson';
import { TimeManager } from './time-manager';

export class LessonMatrix {

    public matrix: any = {}
    public startDate:Date
    public endDate:Date
    public timeLine: string[] = []

  

    public constructor( start:number ){
        for(let i = 0 ; i<24; i++ ){
            this.matrix[this.getTime(i)] = this.getEmptyWeek(start)
        }
    }


    public getStartDate(){
        
    }

    public getEndDate(){

    }




    public add(l:Lesson){

    }

    public getLessonCell(time:string){
           return  `${time.split(":")[0]}:00`
    }
    public addArray(lessons:Lesson[]){
        lessons.forEach(l => {
            this.timeLine.push(this.getLessonCell(l.startTime))
            this.matrix[this.getLessonCell(l.startTime)][l.getDate()].push(l)
        })  

    }

    public checkTime(time:string){
        return this.timeLine.indexOf(time) != -1 
    }
    
    public getEmptyWeek(start:number){
        let result:any = {}
        for(let i = 0; i < 7; i++) result[i + start] = []
        return result 
    }
    
    
    public getTime(i:number):string{
        let houre:number = (i + 7) % 24
        return houre >= 10 ? `${houre}:00` : `0${houre}:00` ;
    }

    isFilteredByTab:Boolean = false
    filteredTab:any = {id:-1,name:'Направления'}

    public count(time:string):number{
        let count = 0;
        //for(let key in this.matrix){
          
            for(let key in this.matrix[time]){
            let lessons = this.matrix[time][key]
          
                count += lessons.filter((l)=>{
                    if(this.isFilteredByTab){
                       
                       return  l.tab_id == this.filteredTab.id
                    }

                    return true;
                }).length
            }
      //  }
      return count
    }

    public filterLesson(l:Lesson){
        console.log(this.isFilteredByTab)
        if(this.isFilteredByTab){
                       
            return  l.tab_id == this.filteredTab.id
         }

         return true;
    }
}

