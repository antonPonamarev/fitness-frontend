import { Component, OnInit } from '@angular/core';
import { WidgetsService, Status, Pending } from 'src/app/services/widgets.service';
import { Membership } from 'src/app/classes/membership';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-shop-page',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.css']
})
export class ShopPageComponent implements OnInit {
  
  constructor(private ts: WidgetsService, private ar: ActivatedRoute,private router: Router) { 
    
  }


  public currentCard: Membership
  public isShowModal: Boolean = false


  public Status = Status;
  public cards :Pending<Membership[]>


  ngOnInit(): void {
  
    this.cards = this.ts.getCards()
    //this.ts.currentCards['1212']= new Membership()
  }
  

  public showModal(t:Membership):void{
    console.log(t)
    this.router.navigate([{outlets: {['modal']:['modal','payment','empty',t.id]}}],{queryParams:{'backurl':`modal/payment/card/${t.id}`}})
  }

  public hideModal():void{
    this.isShowModal = false
  }
}
