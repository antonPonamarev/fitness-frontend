import { Component, OnInit, Input } from '@angular/core';
import { TimeManager, Day } from 'src/app/classes/time-manager';
import { LessonMatrix } from 'src/app/classes/lesson-matrix';
import { WidgetEntity } from 'src/app/services/widgets.service';
import { Lesson } from 'src/app/classes/lesson';
import { ModalEntity, ModalType } from 'src/app/components/modal/common-modal/common-modal.component';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-lessons-page',
  templateUrl: './lessons-page.component.html',
  styleUrls: ['./lessons-page.component.css','../widget-page.component.css']
})
export class LessonsPageComponent implements OnInit {


  public modal: ModalEntity
  public weekDays:Day[]
  public currentLesson:Lesson
  public isShowModal:Boolean = false
  @Input() widget:WidgetEntity
  public isPlaceHolder: Boolean = false

  constructor(private ms:ModalService) { 
  }

  ngOnInit(): void {
    this.weekDays = TimeManager.getWeekDays()
    if(this.widget === undefined){
      this.widget = {
        trainers : [],
        schedule: new LessonMatrix(21),
        lessons: [],
        tabs:[]
      }
      this.isPlaceHolder = true
    } 
  }

  public showLesson(lesson:Lesson):void{
    this.ms.openLesson(lesson)
  }
  public resetFilteredByTab(tab:any):void{
    this.widget.schedule.isFilteredByTab = false;
  }

  public filteredByTab(tab:any):void{
    this.widget.schedule.isFilteredByTab = true;
    this.widget.schedule.filteredTab = tab
  }

}
