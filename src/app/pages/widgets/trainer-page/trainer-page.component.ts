import { Component, OnInit } from '@angular/core';
import { WidgetsService, Pending, Status } from 'src/app/services/widgets.service';
import { WidgetEntity } from '../../../services/widgets.service';
import { Lesson } from 'src/app/classes/lesson';
import { Trainer } from 'src/app/classes/trainer';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css','../widget-page.component.css']
})
export class TrainerPageComponent implements OnInit {
  
  constructor(private ts: WidgetsService) { 
    
  }


  public currentTrainer: Trainer
  public isShowModal: Boolean = false


  public Status = Status;
  public trainers :Pending<Lesson[]>


  ngOnInit(): void {
    this.trainers = this.ts.getTrainers()
  }
  

  public showModal(t:Trainer):void{
    this.currentTrainer = t;
    this.isShowModal = true
  }

  public hideModal():void{
    this.isShowModal = false
  }
}
