import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonalService } from 'src/app/services/personal.service';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-verification-document',
  templateUrl: './verification-document.component.html',
  styleUrls: ['./verification-document.component.css','../personal-style.css']
})
export class VerificationDocumentComponent implements OnInit {

  constructor(private ps: PersonalService, private ar: ActivatedRoute,private router: Router) { }

  public isPassportLoadPreview:Boolean = false
  public isPassportError:Boolean = false
  public isPassportSuccess:Boolean = false
public isPassportErrorPreview:Boolean = false
  public isAddressError:Boolean = false

  public passportImage:any
  public address:string = ""
  ngOnInit(): void {
    console.log(this.ar.snapshot.queryParamMap.get('backurl'))
  }
  public photo:any
  public updateUserPassport(event):void{
    this.isPassportError = false
    this.isPassportErrorPreview = false
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    let $self = this
    reader.onload = function (e) {
      $self.isPassportLoadPreview = true
      $self.passportImage ={'background-image':`url(${ e.target.result})`}
  };
 
    this.photo = event
  }
public changeAddress():void{
  this.isAddressError = false
}
public showPreloader:Boolean = false
public addressError:Boolean = false

  public sendDocumentComplete():void{
    if(this.ar.snapshot.queryParamMap.has('backurl')){
      this.router.navigate([{outlets: {['modal']:this.ar.snapshot.queryParamMap.get('backurl').split('/')}}])
    }
  }

  public sendDocument(address:NgModel):void{
    
     
      let countError:number = 0
      if(address.value.length == 0){
        this.isAddressError = true
 
        countError++
      
      }

      if(!this.isPassportLoadPreview){
        this.isPassportErrorPreview = true
        countError++
      }

      if(countError > 0 ){
        return
      }
      this.showPreloader = true
 

          this.ps.uploadUserPassport(this.photo,address.value).subscribe((r:any) => {
            
        if(r.type == 4){
          //this.isPassportLoad = true
      
          if(r.body.status == "Verified"){
            this.isPassportSuccess = true
            this.showPreloader = false
     
          }else{
            this.isPassportError = true
            this.showPreloader = false
            this.isPassportErrorPreview = true
          }
   
        }


      })

  }
}
