import { Component, OnInit } from '@angular/core';
import { PersonalService } from 'src/app/services/personal.service';
import { Pending } from 'src/app/services/widgets.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { NgModel} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Club } from 'src/app/classes/club';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css', '../personal-style.css']
})
export class LoginPageComponent implements OnInit {

  constructor(private ps: PersonalService,private ar: ActivatedRoute, private router: Router ) { }

  isAgrements: Boolean = false
  isAgrementsError: Boolean = false
  isErrorCode: Boolean = false
  isErrorPhone: Boolean = false
  isCodeSend:Boolean = false
  phone:string = "" 
  code:string = ""
  getCodeStatus :Observable<any>
  showCodeBtn:Boolean = false
  getCodeInterval:any
  clubs:Pending<Club[]>
  isPreloader:Boolean = false


  ngOnInit(): void {
    this.selectedClub = new Club()
    this.clubs = this.ps.getClubs()
    console.log(this.ar.snapshot.queryParams)
    
  }

  selectedClub:Club

  public selectClub(club:Club){
    this.selectedClub = club
  }

  timerGetCode: number = 30

  public getCode(phone:NgModel):void{

    if(!this.isAgrements){
      this.isAgrementsError = true
      return 
    }

    if(phone.value.length < 8){
        this.isErrorPhone = true
        return
    }


    let phoneValue= this.clearPhone(phone.value)
    this.ps.getSecretCode(phoneValue).subscribe()
    this.showCodeBtn = true
    this.timerGetCode = 30
    clearInterval(this.getCodeInterval)
    this.getCodeInterval = setInterval( () => {this.timerGetCode--},1000 )    
  }
public changeUser(){
  this.ps.userChange(12)
}
public sendCode():void{
  this.isPreloader = true
  this.isCodeSend = true
  let phoneValue:string= this.clearPhone(this.phone)
  let codeValue:string = this.code

  this.ps.sendSecretCode(phoneValue,codeValue,"2").subscribe((r:any) => {
    let token:string = r.headers.get('token')
    this.isPreloader = false
    localStorage.setItem('token', token);

    //решаем куда дальше двигаем, в зависимости от backurl
    this.isCodeSend = false
    if(this.ar.snapshot.queryParamMap.has('backurl')){
      let queryParams = this.ar.snapshot.queryParamMap.get('backurl').split('/')
      
      if(queryParams[1] == 'payment'){
        //this.router.navigate(['modal','verification','user']);
        this.router.navigate([{outlets: {['modal']:this.ar.snapshot.queryParamMap.get('backurl').split('/')}}])
        return 
      }
      
    }

    this.router.navigate([{outlets: {['modal']:['modal','personal','main']}}])
    
  },(error)=>{
    this.isPreloader = false
    this.isErrorCode = true
    this.isCodeSend = false
    console.log(error)
  })

}

private clearPhone(phone:string){
  return "7"+phone//.replaceAll("-", "").replaceAll("(", "").replaceAll(")", "").replaceAll(" ", "").replaceAll("+", "");
}

public changePhone():void{
  this.isErrorPhone = false
}
public changeCode():void{
  this.isErrorCode = false
}

  setAgriments():void{
    this.isAgrementsError = false
    this.isAgrements = !this.isAgrements
  }
}
