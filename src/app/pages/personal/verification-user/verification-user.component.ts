import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonalService } from 'src/app/services/personal.service';
import { Pending, Status } from 'src/app/services/widgets.service';
import { NgModel} from '@angular/forms';

@Component({
  selector: 'app-verification-user',
  templateUrl: './verification-user.component.html',
  styleUrls: ['./verification-user.component.css', '../personal-style.css']
})
export class VerificationUserComponent implements OnInit {

  constructor( private ps: PersonalService,private ar: ActivatedRoute,  private router: Router) { }

  public userPending:Pending<any>
  //public user:any
  ngOnInit(): void {
    this.userPending = this.ps.getUser()
  }

  public photo:any
  public previewImage:any 
  public isPreview:Boolean = false
  public isPreloader:Boolean = false
  public status = Status
  public isErrorEmail:Boolean = false
  public errorText:string = ""


  public setPreview(event,user):void{
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    let $self = this
    reader.onload = function (e) {
      $self.isPreview = true
      user.image =e.target.result
  };
 
    this.photo = event
  }

  public updateUserImage(user:any):void{
  


    if(user.email == ""){
      this.isErrorEmail = true;
      return;
    }
    this.isPreloader = true
    if(!this.isPreview) {
      this.updateUser(user)
      return
    }



    this.ps.uploadUserImage(this.photo,user).subscribe((r:any) => {
      console.log(r)
        if(r.type == 4){
          this.isPreloader = false
          this.router.navigate([{outlets: {'modal':['modal','verification','photo']}}],{queryParams:this.ar.snapshot.queryParams})
          //user.image = r.body.image
        }


},(error)=>{
  
  this.errorText = error.error.email[0];
  this.isErrorEmail = true;
  this.isPreloader = false
  console.log(error.error.email[0])})
  }

  public changeEmail(user):void{
    if(user.email.indexOf("@")!=-1 && user.email.indexOf(".")!=-1){
      this.isErrorEmail = false;
      this.errorText = ""
    }
      
  }

  public updateUser(user:any):void{
   
    this.ps.updateUser(user).subscribe((r:any)=>{
      
      this.router.navigate([{outlets: {'modal':['modal','verification','photo']}}],{queryParams:this.ar.snapshot.queryParams})
      this.isPreloader = false
      console.log(r)
    },(error:any) => {
      this.errorText = error;
      this.isErrorEmail = true;
      this.isPreloader = false

    })
    
  }

}
