import { Component, OnInit } from '@angular/core';
import { PersonalService } from 'src/app/services/personal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Pending, Status } from 'src/app/services/widgets.service';

@Component({
  selector: 'app-verification-photo',
  templateUrl: './verification-photo.component.html',
  styleUrls: ['./verification-photo.component.css','../personal-style.css']
})
export class VerificationPhotoComponent implements OnInit {

  constructor(private ps: PersonalService,private ar: ActivatedRoute, private router: Router) { }

  public userPending:Pending<any>

  public isPreview:Boolean = false
  public isPreloader:Boolean = false
  public isError:Boolean = false
  public status = Status
  ngOnInit(): void {
    this.userPending = this.ps.getUser()
  }


  photo:any

  public setPreview(event,user):void{
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    let $self = this
    reader.onload = function (e) {
      $self.isPreview = true
      user.image =e.target.result
  };
 
    this.photo = event
  }



  public sendPhoto(event):void{
    this.ps.uploadFile(event)
  }


  public pageDocument(user:any):void{
    
    
    if(user.image.indexOf('media/default.png') != -1){
      this.isError = true
      return
    }


    this.isPreloader = true
 
    if(this.isPreview){
        this.ps.uploadUserSelfi(this.photo).subscribe((r:any) => {
          
          if(r.type == 4){
            this.isPreloader = false
            //user.image = r.body.image
            this.router.navigate([{outlets: {'modal':['modal','verification','document']}}],{queryParams:this.ar.snapshot.queryParams})
          }


        })
    }
    else{
      this.ps.sendEmptySelfi().subscribe(()=>{
        this.isPreloader = false
        this.router.navigate([{outlets: {'modal':['modal','verification','document']}}],{queryParams:this.ar.snapshot.queryParams})
      })
    }


  }

  public updateUserImage(event:any, user:any):void{
    this.isPreloader = true
    this.isError = false
    this.ps.uploadUserSelfi(event).subscribe((r:any) => {
      
      if(r.type == 4){
        this.isPreloader = false
        user.image = r.body.image
      }


    })
  }



  
}
