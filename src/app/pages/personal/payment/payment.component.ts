import { Component, OnInit } from '@angular/core';
import { Pending, WidgetsService, Status } from 'src/app/services/widgets.service';
import { Membership } from 'src/app/classes/membership';
import { PersonalService } from 'src/app/services/personal.service';
import { ActivatedRoute } from '@angular/router';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { windowWhen } from 'rxjs/operators';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css','../personal-style.css']
})
export class PaymentComponent implements OnInit {

  constructor(private ts: WidgetsService, private ps:PersonalService, private ar: ActivatedRoute, public sanitizer: DomSanitizer) { 

    this.id = this.ar.snapshot.paramMap.get('id')
    this.cards = this.ts.currentCards
  }
  isAgrements:Boolean = true
  isAgrementsError:Boolean = false
  urlSafe: SafeResourceUrl = 'finish.html'
  showFrame:Boolean = false
  cards:any
  id:string
  card:Membership

  intervalWait:any
  isPreloader:Boolean = true
  public status = Status
  ngOnInit(): void {
    this.card = this.ts.getCardById(this.id)
      this.intervalWait = setInterval(()=>{
        if(this.ts.waitCardById(this.id)){
          this.card = this.ts.getCardById(this.id)
          this.isPreloader = false
          clearInterval(this.intervalWait)
        }
        
      },100)
     
  }
  public isEmpty:Boolean = true


  getPaymentLink(card:Membership):void{
    this.ps.getPaymentLink(card).subscribe((r)=>{
      this.showFrame = true
      //window.open(r)
      this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(r)
    })

  }

  setAgriments():void{
    this.isAgrementsError = false
    this.isAgrements = !this.isAgrements
  }

}
