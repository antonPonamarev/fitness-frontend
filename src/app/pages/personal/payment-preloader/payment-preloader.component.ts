import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment-preloader',
  templateUrl: './payment-preloader.component.html',
  styleUrls: ['./payment-preloader.component.css','../personal-style.css']
})
export class PaymentPreloaderComponent implements OnInit {

  constructor(private router: Router,private ar: ActivatedRoute,) { }

  ngOnInit(): void {

    let id:string = this.ar.snapshot.paramMap.get('id')
    setTimeout(()=>{
      this.router.navigate([{outlets: {['modal']:['modal','payment','card',id]}}],{queryParams:{'backurl':`modal/payment/card/${id}`}})
    },1000)
  }

}
