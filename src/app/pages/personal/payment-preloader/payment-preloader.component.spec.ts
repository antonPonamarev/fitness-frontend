import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentPreloaderComponent } from './payment-preloader.component';

describe('PaymentPreloaderComponent', () => {
  let component: PaymentPreloaderComponent;
  let fixture: ComponentFixture<PaymentPreloaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentPreloaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentPreloaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
