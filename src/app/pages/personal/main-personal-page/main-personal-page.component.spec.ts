import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPersonalPageComponent } from './main-personal-page.component';

describe('MainPersonalPageComponent', () => {
  let component: MainPersonalPageComponent;
  let fixture: ComponentFixture<MainPersonalPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPersonalPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPersonalPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
