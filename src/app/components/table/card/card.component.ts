import { Component, OnInit, Input } from '@angular/core';
import { Membership } from 'src/app/classes/membership';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  isPlaceHolder:Boolean = false
  @Input() card:Membership
  constructor() {


   }

  ngOnInit(): void {
    console.log(this.card)
    if(this.card === undefined || this.card.id == '-1'){
      this.isPlaceHolder = true
      this.card = new Membership()
    }
  }

}
