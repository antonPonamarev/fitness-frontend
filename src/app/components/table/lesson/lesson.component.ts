import { Component, OnInit, Input } from '@angular/core';
import { Lesson } from 'src/app/classes/lesson';

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.css']
})
export class LessonComponent implements OnInit {

  constructor() { }

  @Input() lesson: Lesson;
  
  ngOnInit(): void {
    console.log(this.lesson)
  }

}
