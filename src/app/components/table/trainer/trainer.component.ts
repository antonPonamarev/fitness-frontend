import { Component, OnInit,Input, Output  } from '@angular/core';
import { Trainer } from '../../../classes/trainer';
import { EventEmitter } from 'protractor';
import { TimeManager } from 'src/app/classes/time-manager';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  @Input() trainer:Trainer;
 
  public isPlaceHolder:Boolean = false

  constructor() { 
      

  }



  ngOnInit(): void {
    if(this.trainer === undefined){
      this.trainer = new Trainer()
      this.isPlaceHolder = true
    } 
  }
  public getAvatarStyle(){
    return {backgroundImage : `url(${this.trainer.image_url})`}
  }
}

