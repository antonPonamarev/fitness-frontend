import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestLessonComponent } from './test-lesson.component';

describe('TestLessonComponent', () => {
  let component: TestLessonComponent;
  let fixture: ComponentFixture<TestLessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestLessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
