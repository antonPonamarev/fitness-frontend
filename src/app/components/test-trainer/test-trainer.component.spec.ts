import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestTrainerComponent } from './test-trainer.component';

describe('TestTrainerComponent', () => {
  let component: TestTrainerComponent;
  let fixture: ComponentFixture<TestTrainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestTrainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTrainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
