import { Component, OnInit } from '@angular/core';
import { PersonalService } from 'src/app/services/personal.service';
import { Pending, Status } from 'src/app/services/widgets.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private ps: PersonalService,private router: Router) { }
  userPending:Pending<any>
  ngOnInit(): void {
    this.user = this.ps.getUserFull()
    this.userPending = this.ps.getUser()
  }
  public status = Status;
  public modalAuth():void{
    console.log("клик")
    this.router.navigate([{outlets: {['modal']:['modal','login']}}])
  }

  public user:any = {
    id: 12
  }

  public modalPersonal():void{
    this.router.navigate([{outlets: {['modal']:['modal','personal','main']}}])
  }
  public changeUser(){
    this.ps.userChange(25)
  }
  public getBackground(user:any){
    return {
      'background-image' : `url(${user.image})`
    }
  }
}
