import { Component, OnInit, Input } from '@angular/core';
import { Trainer } from 'src/app/classes/trainer';
import { Lesson } from 'src/app/classes/lesson';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-common-modal',
  templateUrl: './common-modal.component.html',
  styleUrls: ['./common-modal.component.css']
})
export class CommonModalComponent implements OnInit {


  public type = ModalType

  constructor(public ms:ModalService) { }

  ngOnInit(): void {

  }

}


export interface ModalEntity{
  type:ModalType,
  lesson?: Lesson,
  trainer?:Trainer
}


export enum ModalType {
  lesson = 'lesson',
  trainer = 'trainer',
  empty = 'empty'
}
