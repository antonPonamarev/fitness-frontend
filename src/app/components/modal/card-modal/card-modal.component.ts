import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Membership } from 'src/app/classes/membership';
import { WidgetsService } from 'src/app/services/widgets.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-modal',
  templateUrl: './card-modal.component.html',
  styleUrls: ['./card-modal.component.css','/src/app/pages/personal/personal-style.css']
})
export class CardModalComponent implements OnInit {

  @Input() card:Membership

  @Output() isClosed: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private ts: WidgetsService,private router: Router) { }
  ngOnInit(): void {
  }

public buy(card:Membership){
  
  this.router.navigate([{outlets: {['modal']:['modal','payment','card',card.id]}}],{queryParams:{'backurl':`modal/payment/card/${card.id}`}})
  this.hideModal()
}

  public hideModal():void{
    this.isClosed.emit(false)
    //this.widget.show = false
  }
}
