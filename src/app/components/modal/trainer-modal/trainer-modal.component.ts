import { Component, OnInit , Input, EventEmitter, Output} from '@angular/core';
import { Trainer } from 'src/app/classes/trainer';
import { Status, WidgetsService } from 'src/app/services/widgets.service';
import { ScheduleService } from 'src/app/services/schedule.service';

@Component({
  selector: 'app-trainer-modal',
  templateUrl: './trainer-modal.component.html',
  styleUrls: ['./trainer-modal.component.css']
})
export class TrainerModalComponent implements OnInit {

 
  @Input() trainer:Trainer


  constructor(private ts: ScheduleService) { }
  public services:any
  ngOnInit(): void {
    this.services = this.ts.getTrainerService(this.trainer.id)
  }
  public Status = Status;

  public carousel = {
    state: 0,
    navStyle:{
      left:'0%'
    },
    sliderStyle:{
      marginLeft : '0%'
    }
  }




  public clickNav(num:number){
    this.carousel.state = num
    this.carousel.sliderStyle.marginLeft = `-${num * 100}%`
    this.carousel.navStyle.left = `${num * 50}%`
  }



}
