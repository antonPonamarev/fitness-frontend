import { Component, OnInit, Input } from '@angular/core';
import { Lesson } from 'src/app/classes/lesson';
import { ScheduleService } from 'src/app/services/schedule.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-lesson-modal',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.css']
})
export class LessonModalComponent implements OnInit {

  constructor(private sp:ScheduleService, private ms:ModalService) { }

  @Input() lesson:Lesson

  ngOnInit(): void {
  }

  public hideModal(){

  }


  public openTrainerModal(){
    this.ms.openTrainer(this.lesson.coach)
  }

  public applyForGroup(l:Lesson):void{
   
    this.sp.applyForGroupLesson(l).subscribe((r)=>{
    
    })
  }

}
