const fs = require('fs-extra');
const concat = require('concat'); 
(async function build() {    
const files = [
        './dist/fitness-kit/polyfills-es5.js',
        './dist/fitness-kit/main-es5.js', 
]
await fs.ensureDir('builder/widgets')    
await concat(files, 'builder/widgets/fitness-kit-lesson.js')})()